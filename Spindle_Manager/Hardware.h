#include <Tiny4kOLED.h>
#include "Servo8Bit.h" // Initialise Servo library

#define PULSE_PIN 3
#define MOTOR_PIN 1
#define STATE_SELECT_PIN A2

#define STATE_OFF 0 
#define STATE_ESC 1 
#define STATE_LASER 2 
#define STATE_UNDEFINED 99
#define STATE_BOOT 98

#define STATE_OFF_LIMIT 220
#define STATE_ESC_LIMIT 600
#define STATE_LASER_LIMIT 990
#define XPos(x) (x*6)

// convert in define
uint8_t offset = 50;
uint16_t minSign = 0;
uint16_t maxSign = 800;
uint16_t minBrush = 44;
uint16_t maxBrush = 180;

struct Status {
  uint8_t machineState;
  uint8_t lastMachineState;
  uint16_t rxPulse;
  uint16_t rxPulseOld;
  uint16_t rxPulseConstraint;
  uint16_t motorValue;
} StatusData = {
  STATE_BOOT,
  STATE_BOOT,
  0,
  0
};
