#include "Hardware.h"

uint16_t rxPulse = 0;  // Rx signal variable
uint16_t rxPulseOld = 0;  // Rx signal variable
uint16_t rxPulseConstraint;  // Rx signal variable         
uint16_t escPulse;  // Rx signal variable         

void setup() {

  // pinMode (PULSE_PIN, INPUT);    //Attach Rx signal PB4 (pin3 on ATtiny45/85)
  pinMode (STATE_SELECT_PIN, INPUT);
  pinMode (PULSE_PIN, INPUT);    //Attach Rx signal PB4 (pin3 on ATtiny45/85)
  
  InitOledScreen();

  //first read
  rxPulse = pulseIn(PULSE_PIN,HIGH);
  rxPulseOld = rxPulse;
  
  // ESC.attach (MOTOR_PIN);    // Attach ESC signal to PB1 (pin6 on ATtiny45/85)
  // ESC.write(minBrush);    // Output generated signals to ESC

}

// the loop routine runs over and over again forever:
void loop() {

  // checked every loop, to avoid delay in switch off
  uint16_t sensorValue = CheckState();

  if(StatusData.machineState != StatusData.lastMachineState){
    // Updates state view, only on change
    
    StatusData.lastMachineState = StatusData.machineState;
  }

  oled.clear();
  
  ActionOnState();

  // only for debug  
  // oled.setCursor(0, 3);
  // oled.print(sensorValue);
  
  // float voltage = sensorValue * (5.0 / 1023.0);
  
  // oled.setCursor(XPos(6), 3);
  // oled.print(voltage);

  // almost every state prints somethig
  oled.switchFrame();
  
}

void InitOledScreen(){
  oled.begin();
  oled.setFont(FONT6X8);
  // Clear the memory before turning on the display
  oled.clear();
  // Turn on the display
  oled.on();
  // Switch the half of RAM that we are writing to, to be the half that is non currently displayed
  oled.switchRenderFrame();
};


uint16_t CheckState(){
   
  // read the input on analog pin:
  uint16_t selectPinValue = analogRead(STATE_SELECT_PIN);
  
  if(selectPinValue <= STATE_OFF_LIMIT) {
    StatusData.machineState = STATE_OFF;
  }
  else if(selectPinValue <= STATE_ESC_LIMIT) {
    StatusData.machineState = STATE_ESC;
  }
  else if(selectPinValue <= STATE_LASER_LIMIT) {
    StatusData.machineState = STATE_LASER;
  }else{
    StatusData.machineState = STATE_UNDEFINED;
  }

  return selectPinValue;
}

void ActionOnState(){
  
  // Current State must always be printed
  oled.setCursor(0, 0);
  oled.print(F("State: "));
  oled.setCursor(XPos(7), 0);
  oled.print(StateToString(StatusData.machineState));

  if(StatusData.machineState == STATE_OFF) {
    // all off
    OffState();
  }
  else if(StatusData.machineState == STATE_ESC) {
    EscState();
  }
  else if(StatusData.machineState == STATE_LASER) {
    LaserState();
  }else{
    OffState();
    oled.setCursor(1, 2);
    oled.print(F("MAJOR ERROR"));
  }

}

void OffState(){

  //turns everything off
}

void EscState(){

  rxPulse = pulseIn(PULSE_PIN,HIGH);

  // check if the value is new
  if(rxPulseOld == rxPulse || (rxPulseOld - offset <= rxPulse && rxPulse <= rxPulseOld + offset)){
    oled.setCursor(1, 1);
    oled.print(F("Raw: "));
    oled.setCursor(XPos(0), 2);
    oled.print(rxPulse);

    oled.setCursor(XPos(5), 1);
    oled.print(F("Const "));
    oled.setCursor(XPos(5), 2);
    oled.print(rxPulseConstraint);

    oled.setCursor(XPos(11), 1);
    oled.print(F("ESC: "));
    oled.setCursor(XPos(11), 2);
    oled.print(escPulse);

    return;
  }
  
  rxPulseOld = rxPulse;

  rxPulseConstraint = constrain(rxPulse, minSign, maxSign);    // Ignore signal values outside expected range                                         
 
  escPulse = map(rxPulseConstraint, minSign, maxSign, minBrush, maxBrush);    // Map input signal to expected output signal range

  // ESC.write(escPulse);    // Output generated signals to ESC


  
}

void LaserState(){
  
  oled.setCursor(XPos(2), 1);
  oled.print(F("Power Output: "));
  oled.setCursor(XPos(9), 2);
  oled.print(800);

}

String StateToString(uint8_t state){
  if(state == STATE_OFF) {
    return "OFF";
  }
  else if(state == STATE_ESC) {
    return "Drill Mode";
  }
  else if(state == STATE_LASER) {
    return "Laser Mode";
  }
  else {
    return "UNDEFINED";
  }

}