#include <Tiny4kOLED.h>

void setup() {
  
  oled.begin();
  oled.setFont(FONT6X8);
  // Clear the memory before turning on the display
  oled.clear();
  // Turn on the display
  oled.on();
  // Switch the half of RAM that we are writing to, to be the half that is non currently displayed
  oled.switchRenderFrame();

}

void loop() {
  // put your main code here, to run repeatedly:

    oled.clear();
    oled.setCursor(0, 0);
    oled.print(millis());
    oled.switchFrame();

    
}
